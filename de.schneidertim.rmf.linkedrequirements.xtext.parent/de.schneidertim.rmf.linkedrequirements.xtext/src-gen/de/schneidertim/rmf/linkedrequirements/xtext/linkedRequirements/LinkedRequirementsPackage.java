/**
 * generated by Xtext 2.15.0
 */
package de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.LinkedRequirementsFactory
 * @model kind="package"
 * @generated
 */
public interface LinkedRequirementsPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "linkedRequirements";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.schneidertim.de/rmf/linkedrequirements/xtext/LinkedRequirements";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "linkedRequirements";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  LinkedRequirementsPackage eINSTANCE = de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkedRequirementsPackageImpl.init();

  /**
   * The meta object id for the '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.ModelImpl
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkedRequirementsPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Links</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__LINKS = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkImpl <em>Link</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkImpl
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkedRequirementsPackageImpl#getLink()
   * @generated
   */
  int LINK = 1;

  /**
   * The feature id for the '<em><b>From</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__FROM = 0;

  /**
   * The feature id for the '<em><b>To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__TO = 1;

  /**
   * The number of structural features of the '<em>Link</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Model#getLinks <em>Links</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Links</em>'.
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Model#getLinks()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Links();

  /**
   * Returns the meta object for class '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Link <em>Link</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Link</em>'.
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Link
   * @generated
   */
  EClass getLink();

  /**
   * Returns the meta object for the reference '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Link#getFrom <em>From</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>From</em>'.
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Link#getFrom()
   * @see #getLink()
   * @generated
   */
  EReference getLink_From();

  /**
   * Returns the meta object for the reference '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Link#getTo <em>To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>To</em>'.
   * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.Link#getTo()
   * @see #getLink()
   * @generated
   */
  EReference getLink_To();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  LinkedRequirementsFactory getLinkedRequirementsFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.ModelImpl
     * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkedRequirementsPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Links</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__LINKS = eINSTANCE.getModel_Links();

    /**
     * The meta object literal for the '{@link de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkImpl <em>Link</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkImpl
     * @see de.schneidertim.rmf.linkedrequirements.xtext.linkedRequirements.impl.LinkedRequirementsPackageImpl#getLink()
     * @generated
     */
    EClass LINK = eINSTANCE.getLink();

    /**
     * The meta object literal for the '<em><b>From</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINK__FROM = eINSTANCE.getLink_From();

    /**
     * The meta object literal for the '<em><b>To</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINK__TO = eINSTANCE.getLink_To();

  }

} //LinkedRequirementsPackage
