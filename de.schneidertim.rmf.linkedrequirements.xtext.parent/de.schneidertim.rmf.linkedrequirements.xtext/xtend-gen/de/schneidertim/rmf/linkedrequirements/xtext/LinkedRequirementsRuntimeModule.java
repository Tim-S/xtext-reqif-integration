/**
 * generated by Xtext 2.15.0
 */
package de.schneidertim.rmf.linkedrequirements.xtext;

import de.schneidertim.rmf.linkedrequirements.xtext.AbstractLinkedRequirementsRuntimeModule;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
@SuppressWarnings("all")
public class LinkedRequirementsRuntimeModule extends AbstractLinkedRequirementsRuntimeModule {
}
