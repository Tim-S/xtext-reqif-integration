/**
 * generated by Xtext 2.15.0
 */
package de.schneidertim.rmf.linkedrequirements.xtext;

import de.schneidertim.rmf.linkedrequirements.xtext.LinkedRequirementsStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
@SuppressWarnings("all")
public class LinkedRequirementsStandaloneSetup extends LinkedRequirementsStandaloneSetupGenerated {
  public static void doSetup() {
    new LinkedRequirementsStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
}
