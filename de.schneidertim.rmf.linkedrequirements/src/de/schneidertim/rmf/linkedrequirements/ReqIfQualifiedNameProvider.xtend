package de.schneidertim.rmf.linkedrequirements

import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider
import org.eclipse.emf.ecore.EObject
import org.eclipse.rmf.reqif10.SpecObject
import org.eclipse.xtext.naming.QualifiedName

class ReqIfQualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {
	override getFullyQualifiedName(EObject obj) {
		switch (obj) {
			SpecObject: return QualifiedName.create(obj.longName)
			default: return super.getFullyQualifiedName(obj)
		}
	}
}