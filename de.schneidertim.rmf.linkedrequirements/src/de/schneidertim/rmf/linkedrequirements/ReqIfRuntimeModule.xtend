package de.schneidertim.rmf.linkedrequirements

import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule

class ReqIfRuntimeModule extends AbstractGenericResourceRuntimeModule {
	
	override protected getFileExtensions() {
		'reqif'
	}
	
	override protected getLanguageName() {
		'de.schneidertim.rmf.linkedrequirements.LinkedRequirements'
	}
	
	override bindIQualifiedNameProvider() {
		ReqIfQualifiedNameProvider
	}
}