package de.schneidertim.rmf.linkedrequirements

import com.google.inject.Guice
import com.google.inject.Inject
import org.eclipse.xtext.ISetup
import org.eclipse.xtext.resource.FileExtensionProvider
import org.eclipse.xtext.resource.IResourceServiceProvider
import org.eclipse.rmf.reqif10.ReqIF10Package

class ReqIfStandaloneSetup implements ISetup {
	
	@Inject
	FileExtensionProvider fileExtensionProvider;

	@Inject
	IResourceServiceProvider resourceServiceProvider;

	@Inject
	IResourceServiceProvider.Registry registry;
	
	override createInjectorAndDoEMFRegistration() {
		val injector = Guice.createInjector(new ReqIfRuntimeModule)
		injector.injectMembers(this)
		for(fileExt: fileExtensionProvider.fileExtensions) 
			registry.extensionToFactoryMap.put(fileExt, resourceServiceProvider)

		// the following implicitly registers the EPackage to the registry
		val reqifPackage = ReqIF10Package.eINSTANCE
		
		return injector
	}
	
}