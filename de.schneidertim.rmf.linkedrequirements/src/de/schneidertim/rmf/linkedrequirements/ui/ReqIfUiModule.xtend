package de.schneidertim.rmf.linkedrequirements.ui

import com.google.inject.Binder
import org.eclipse.ui.plugin.AbstractUIPlugin
import org.eclipse.xtext.ui.resource.generic.EmfUiModule
import org.eclipse.xtext.ui.editor.IURIEditorOpener
import org.eclipse.xtext.ui.LanguageSpecific
import org.eclipse.xtext.ui.editor.LanguageSpecificURIEditorOpener
import org.eclipse.emf.edit.ui.util.EditUIUtil
import org.eclipse.xtext.ui.ecore.EcoreEditorOpener

class ReqIfUiModule extends EmfUiModule {
	new(AbstractUIPlugin plugin) {
		super(plugin)
	}
	
	override configureLanguageSpecificURIEditorOpener(Binder binder) {
		//binder.bind(IURIEditorOpener).annotatedWith(LanguageSpecific).to(ReqIfEditorOpener)
		//binder.bind(IURIEditorOpener).annotatedWith(LanguageSpecific).to(EcoreEditorOpener);
	}
}