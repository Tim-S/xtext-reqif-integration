package de.schneidertim.rmf.linkedrequirements;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.rmf.reqif10.SpecObject;
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;

@SuppressWarnings("all")
public class ReqIfQualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {
  @Override
  public QualifiedName getFullyQualifiedName(final EObject obj) {
    boolean _matched = false;
    if (obj instanceof SpecObject) {
      _matched=true;
      return QualifiedName.create(((SpecObject)obj).getLongName());
    }
    return super.getFullyQualifiedName(obj);
  }
}
