package de.schneidertim.rmf.linkedrequirements;

import de.schneidertim.rmf.linkedrequirements.ReqIfQualifiedNameProvider;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;

@SuppressWarnings("all")
public class ReqIfRuntimeModule extends AbstractGenericResourceRuntimeModule {
  @Override
  protected String getFileExtensions() {
    return "reqif";
  }
  
  @Override
  protected String getLanguageName() {
    return "de.schneidertim.rmf.linkedrequirements.LinkedRequirements";
  }
  
  @Override
  public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
    return ReqIfQualifiedNameProvider.class;
  }
}
