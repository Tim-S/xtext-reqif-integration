package de.schneidertim.rmf.linkedrequirements;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import de.schneidertim.rmf.linkedrequirements.ReqIfRuntimeModule;
import java.util.Set;
import org.eclipse.rmf.reqif10.ReqIF10Package;
import org.eclipse.xtext.ISetup;
import org.eclipse.xtext.resource.FileExtensionProvider;
import org.eclipse.xtext.resource.IResourceServiceProvider;

@SuppressWarnings("all")
public class ReqIfStandaloneSetup implements ISetup {
  @Inject
  private FileExtensionProvider fileExtensionProvider;
  
  @Inject
  private IResourceServiceProvider resourceServiceProvider;
  
  @Inject
  private IResourceServiceProvider.Registry registry;
  
  @Override
  public Injector createInjectorAndDoEMFRegistration() {
    ReqIfRuntimeModule _reqIfRuntimeModule = new ReqIfRuntimeModule();
    final Injector injector = Guice.createInjector(_reqIfRuntimeModule);
    injector.injectMembers(this);
    Set<String> _fileExtensions = this.fileExtensionProvider.getFileExtensions();
    for (final String fileExt : _fileExtensions) {
      this.registry.getExtensionToFactoryMap().put(fileExt, this.resourceServiceProvider);
    }
    final ReqIF10Package reqifPackage = ReqIF10Package.eINSTANCE;
    return injector;
  }
}
