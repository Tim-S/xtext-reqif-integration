package de.schneidertim.rmf.linkedrequirements.ui;

import com.google.inject.Injector;
import de.schneidertim.rmf.linkedrequirements.ui.Activator;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

@SuppressWarnings("all")
public class ReqIfExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {
  @Override
  protected Bundle getBundle() {
    return Activator.getDefault().getBundle();
  }
  
  @Override
  protected Injector getInjector() {
    return Activator.getDefault().getInjector();
  }
}
