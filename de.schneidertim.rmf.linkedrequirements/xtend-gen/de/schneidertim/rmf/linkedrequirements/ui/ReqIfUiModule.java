package de.schneidertim.rmf.linkedrequirements.ui;

import com.google.inject.Binder;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.resource.generic.EmfUiModule;

@SuppressWarnings("all")
public class ReqIfUiModule extends EmfUiModule {
  public ReqIfUiModule(final AbstractUIPlugin plugin) {
    super(plugin);
  }
  
  @Override
  public void configureLanguageSpecificURIEditorOpener(final Binder binder) {
  }
}
